CREATE DATABASE gloves_shop;

CREATE TABLE gloves_shop.customer(
    id int NOT NULL AUTO_INCREMENT,
    first_name varchar(50) NOT NULL,
    last_name varchar(50) NOT NULL,
    address varchar(50) NOT NULL,
    email varchar(50) NOT NULL UNIQUE,
    PRIMARY KEY(id)
);

CREATE TABLE gloves_shop.purchase(
    id int NOT NULL AUTO_INCREMENT,
    customer_id int NOT NULL,
    size varchar(50) NOT NULL,
    color varchar(50) NOT NULL,
    quantity int NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(customer_id) REFERENCES customer(id)
);