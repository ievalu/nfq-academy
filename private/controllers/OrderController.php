<?php

class OrderController{

    protected $query;
    protected $sizes = ["S", "M", "L"];
    protected $colors = ["white", "red", "blue", "green", "black"];

    public function __construct($query){
        $this->query = require $query;
    }

    protected function isTestInputValid($email, $size, $color, $quantity){
        if(!(filter_var($email, FILTER_VALIDATE_EMAIL)) ||
            !(in_array($size, $this->sizes)) || !(in_array($color, $this->colors)) || 
            !(is_numeric($quantity))){
                return False;
        }
        return True;
    }

    public function post(){
        $validInput = (isset($_POST["firstName"]) && 
            isset($_POST["lastName"]) &&
            isset($_POST["address"]) &&
            isset($_POST["email"]) &&
            isset($_POST["size"]) &&
            isset($_POST["color"]) &&
            isset($_POST["quantity"]) &&
            $this->isTestInputValid($_POST["email"], $_POST["size"], 
            $_POST["color"], $_POST["quantity"]));
        $successfullPurchase = false;
        if($validInput){
            $customer = $this->query->selectCustomerByEmail($_POST["email"]);
            if($customer){
                $this->query->insertToPurchase($customer[0]->id, $_POST["size"], 
                    $_POST["color"], $_POST["quantity"]);
            }
            else{
                $this->query->insertToCustomer($_POST["firstName"], $_POST["lastName"], 
                    $_POST["address"], $_POST["email"]);
                $customer = $this->query->selectCustomerByEmail($_POST["email"]);
                $this->query->insertToPurchase($customer[0]->id, $_POST["size"], 
                $_POST["color"], $_POST["quantity"]);
            }
            $successfullPurchase = true;
        }
        require "private/views/order.view.php";
    }
}