<?php

class PageController{

    protected $query;

    public function __construct($query){
        $this->query = require $query;
    }

    protected function getPageNr(){
        if(isset($_GET['page'])){
            $pageNr = $_GET['page'];
        } 
        else {
            $pageNr = 1;
        }
        return $pageNr;
    }

    protected function getOffsetNumber($nrOfRecords){
        $pageNr = $this->getPageNr();
        $offset = ($pageNr-1) * $nrOfRecords;
        return $offset;
    }

    protected function getSortType(){
        if(isset($_GET["sort"])){
            $sort = $_GET["sort"];
        }
        else{
            $sort = "byPurchaseId";
        }
        return $sort;
    }

    protected function getNrOfRecords(){
        if (isset($_GET['count'])) {
            $nrOfRecords = $_GET['count'];
        } else {
            $nrOfRecords = 8;
        }
        return $nrOfRecords;
    }

    protected function getSearchText(){
        if (isset($_GET['search'])) {
            $search = $_GET['search'];
        } else {
            $search = false;
        }
        return $search;
    }

    protected function getSearchBy(){
        if (isset($_GET['searchBy']) && $_GET['searchBy'] != "") {
            $search = $_GET['searchBy'];
        } else {
            $search = false;
        }
        return $search;
    }

    public function home(){
        require "private/views/home.view.php";
    }

    public function order(){
        require "private/views/order.view.php";
    }

    public function orders(){
        $pageNr = $this->getPageNr();
        $nrOfRecords = $this->getNrOfRecords();
        $offset = $this->getOffsetNumber($nrOfRecords);
        $sort = $this->getSortType();
        $search = $this->getSearchText();
        $searchBy = $this->getSearchBy();
        $totalRecords = $this->query->countRowsPurchases($search, $searchBy);
        $totalPages = ceil($totalRecords/$nrOfRecords);
        $purchases = $this->query->selectPurchase($offset, $nrOfRecords, $sort, $search, $searchBy);
        require "private/views/orders-history.view.php";
    }
}