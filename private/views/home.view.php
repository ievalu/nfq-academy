<html>
    <head>
        <title>Welcome!</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="public-html/css/home.css">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
    </head>
    <body>
        <?php require "private/templates/nav.php"; ?>
        <div class="content">
            <h1 class="greeting">
                Welcome to our shop!
            </h1>
        </div>
    </body>
</html>