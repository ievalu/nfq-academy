<html>
    <head>
        <title>Orders history</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="public-html/css/orders-history.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>
    <body>
        <?php require "private/templates/nav.php"; ?>
        <div class="util">
            <?php require "private/templates/pagination.php"; ?>
        </div>
        <?php if(count($purchases) == 0): ?>
            <div class="no-results">No results found</div>
        <?php else: ?>
            <table class="orders">
                <thead>
                    <tr>
                        <th>Order id</th>
                        <th>Customer email</th>
                        <th>Size</th>
                        <th>Color</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach($purchases as $purchase): ?>
                        <tr>
                            <td><?= $purchase->id ?></td>
                            <td><?= $purchase->email ?></td>
                            <td><?= $purchase->size ?></td>
                            <td><?= $purchase->color ?></td>
                            <td><?= $purchase->quantity ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </body>
</html>