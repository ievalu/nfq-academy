<html>
    <head>
        <title>Your order</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="public-html/css/order.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>
    <body>
        <?php require "private/templates/nav.php"; ?>
        <div class="content">
            <h1>Fill in the form to place your order</h1>   
            <?php if(isset($validInput) && !$validInput): ?>
                <div class="data-error">MAKE SURE THE INFORMATION IS CORRECT</div>
            <?php endif; ?>
            <?php if(isset($successfullPurchase) && $successfullPurchase): ?>
                <div class="success">Thank you for your purchase</div>
            <?php endif; ?>
            <form method="POST" action="/order">
                <div>
                    <div class="title">Information about you</div>
                    <div class="input">
                        <label class="label">First Name</label>
                        <input type="text" name="firstName" class="user-input"> 
                    </div>
                    <div class="input">
                        <label class="label">Last Name</label>
                        <input type="text" name="lastName" class="user-input">
                    </div>
                    <div class="input">
                        <label class="label">Address</label>
                        <input type="text" name="address" class="user-input">
                    </div>
                    <div class="input">
                        <label class="label">Email</label>
                        <input type="text" name="email" class="user-input">
                    </div>
                </div>
                <div>
                    <div class="title">Your order</div>
                    <div class="input">
                        <label class="label">Size</label>
                        <div class="user-input">
                            <label>S</label>
                            <input type="radio" name="size" value="S">
                            <label>M</label>
                            <input type="radio" name="size" value="M">
                            <label>L</label>
                            <input type="radio" name="size" value="L">
                        </div>
                    </div>
                    <div class="input">
                        <label class="label">Color</label>
                        <select class="user-input" name="color">
                            <option value="white">White</option>
                            <option value="red">Red</option>
                            <option value="blue">Blue</option>
                            <option value="green">Green</option>
                            <option value="black">Black</option>
                        </select>
                    </div>
                    <div class="input">
                        <label class="label">Quantity</label>
                        <input type="text" name="quantity" class="user-input">
                    </div>
                </div>
                <button type="submit">Purchase</button>
            </form>
            <div class="shop-item">
                <img src="public-html/img/gloves.jpg" alt="Gloves">
                <div class="item-description">
                    Hello, we are selling innovative work gloves which are not just a whole more durable
                    than your regular ones but also look cute on your hands!<br>
                    You can choose from several different colors as well as from three different sizes.<br>
                    To purchase the best work gloves in the market you just have to fill in the form on 
                    the left and we will deliver them to the address you have provided.<br>
                    <span class="take-care">Take care!</span>
                </div>
            </div>
        </div>
    </body>
</html>