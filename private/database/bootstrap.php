<?php

require "private/database/QueryBuilder.php";
require "private/database/Connection.php";

$config = require "private/config.php";

return new QueryBuilder(Connection::make($config["database"]));