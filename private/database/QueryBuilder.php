<?php

require "private/models/Customer.php";
require "private/models/Purchase.php";

class QueryBuilder{

    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    protected function sortCollumn(&$sort){
        switch ($sort) {
            case 'byPurchaseId':
                $sort = "purchase.id";
                break;
            case 'byEmail':
                $sort = "customer.email";
                break;
            case 'byQuantity':
                $sort = "purchase.quantity";
                break;
        }
    }

    protected function takeRecords(&$nrOfRecordsPerPage){
        switch ($nrOfRecordsPerPage) {
            case 'all':
                $nrOfRecordsPerPage = $this->countRowsPurchases();
                break;
            default:
                break;
        }
    }

    protected function takeSearchBy(&$searchBy){
        switch ($searchBy) {
            case "email":
                $searchBy = "customer.email";
                break;
            case "color":
                $searchBy = "purchase.color";
                break;
            default:
                break;
        }
    }

    public function insertToCustomer($firstName, $lastName, $address, $email){
        $statement = $this->pdo->prepare("INSERT INTO customer (first_name, last_name, address, email) 
            VALUES ('{$firstName}', '{$lastName}', '{$address}', '{$email}')");
        $statement->execute();
    }

    public function insertToPurchase($customerId, $size, $color, $quantity){
        $statement = $this->pdo->prepare("INSERT INTO purchase (customer_id, size, color, quantity) 
            VALUES ('{$customerId}', '{$size}', '{$color}', '{$quantity}')");
        $statement->execute();
    }

    public function selectCustomerByEmail($email){
        $statement = $this->pdo->prepare("SELECT * FROM customer WHERE email='{$email}'");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS, "Customer");
    }

    public function selectPurchase($offset, $nrOfRecordsPerPage, $sort, $search, $searchBy){
        $this->sortCollumn($sort);
        $this->takeRecords($nrOfRecordsPerPage);
        $this->takeSearchBy($searchBy);
        if(!$searchBy){
            $statement = $this->pdo->prepare("SELECT purchase.id, customer.email, purchase.size,
            purchase.color, purchase.quantity FROM purchase INNER JOIN customer ON
            purchase.customer_id = customer.id 
            ORDER BY $sort ASC
            LIMIT {$offset}, {$nrOfRecordsPerPage}");
        }
        else{
            $statement = $this->pdo->prepare("SELECT purchase.id, customer.email, purchase.size,
                purchase.color, purchase.quantity FROM purchase INNER JOIN customer ON
                purchase.customer_id = customer.id 
                WHERE {$searchBy} LIKE '%{$search}%'
                ORDER BY $sort ASC
                LIMIT {$offset}, {$nrOfRecordsPerPage}");
        }
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS, "Purchase");
    }

    public function countRowsPurchases($search, $searchBy){
        $this->takeSearchBy($searchBy);
        if(!$searchBy && !$search){
            $statement = $this->pdo->prepare("SELECT COUNT(*) FROM purchase");
        }
        else if(!$searchBy){
            return 0;
        }
        else{
            $statement = $this->pdo->prepare("SELECT COUNT(*) FROM purchase 
                INNER JOIN customer ON purchase.customer_id = customer.id
                WHERE {$searchBy} LIKE '%{$search}%'");
        }
        $statement->execute();
        return $statement->fetchColumn();
    }
}