<?php

$router->get("", "PageController@home");
$router->get("order", "PageController@order");
$router->get("orders", "PageController@orders");
$router->post("order", "OrderController@post");