<?php

class Router{

    protected $routes = [
        "GET" => [],
        "POST" => []
    ];

    public static function load($file){
        $router = new static;
        require $file;
        return $router;
    }

    public function direct($url, $requestType){
        if(array_key_exists($url, $this->routes[$requestType])){
            $this->callAction(...explode("@", $this->routes[$requestType][$url]));
        }
        else{
            throw new Exception("No route defined for this URL");
        }
    }

    public function callAction($controller, $method){
        if(method_exists($controller, $method)){
            return (new $controller("private/database/bootstrap.php"))->$method();
        }
        else{
            throw new Exception(
                "{$controller} does not respond to {$method} method"
            );
        }
    }

    public function get($url, $controller){
        return $this->routes["GET"][$url] = $controller;
    }

    public function post($url, $controller){
        return $this->routes["POST"][$url] = $controller;
    }
}