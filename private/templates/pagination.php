<html>
    <head>
        <link rel="stylesheet" type="text/css" href="public-html/css/pagination.css">
    </head>
    <body>
        <ul class="pagination">
            <li class="page-item">
                <a href="?page=<?= $pageNr-1 ?>&count=<?= $nrOfRecords ?>&sort=<?= $sort ?>&search=<?= $search ?>&searchBy=<?= $searchBy ?>">
                    &#60;
                </a>
            </li>
            <?php for($i=$pageNr;$i<=$pageNr+2;$i++): ?>
                <li class="page-item">
                    <a href="?page=<?= $i ?>&count=<?= $nrOfRecords ?>&sort=<?= $sort ?>&search=<?= $search ?>&searchBy=<?= $searchBy ?>">
                        <?php if($pageNr == $i): ?>
                            <span style="color:#ff9999"><?= $i ?></span>
                        <?php else: ?>
                            <?= $i ?>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endfor; ?>
            <li class="page-item">
                <a href="?page=<?= $pageNr+1 ?>&count=<?= $nrOfRecords ?>&sort=<?= $sort ?>&search=<?= $search ?>&searchBy=<?= $searchBy ?>">
                    &#62;
                </a>
            </li>
        </ul>
        <form action="/orders" method="GET">
            <div class="page-count">
                <select name="count" id="count" onchange="this.form.submit()">
                    <option value="8" <?= ($nrOfRecords == '8')?"selected":"" ?>>
                        Number of records
                    </option>
                    <option value="5" <?= ($nrOfRecords == '5')?"selected":"" ?>>
                        5
                    </option>
                    <option value="10" <?= ($nrOfRecords == '10')?"selected":"" ?>>
                        10
                    </option>
                    <option value="15" <?= ($nrOfRecords == '15')?"selected":"" ?>>
                        15
                    </option>
                    <option value="25" <?= ($nrOfRecords == '25')?"selected":"" ?>>
                        25
                    </option>
                    <option value="all" <?= ($nrOfRecords == $totalRecords)?"selected":"" ?>>
                        All
                    </option>
                </select>
            </div>
            <div class="sort">
                <select name="sort" id="sort" onchange="this.form.submit()">
                    <option value="byPurchaseId" <?= ($sort == "byPurchaseId")?"selected":"" ?>>
                        Sort
                    </option>
                    <option value="byEmail" <?= ($sort == "byEmail")?"selected":"" ?>>
                        By email
                    </option>
                    <option value="byQuantity" <?= ($sort == "byQuantity")?"selected":"" ?>>
                        By quantity
                    </option>
                </select>
            </div>
            <div class="search">
                <select name="searchBy">
                    <option value="" <?= ($searchBy == "")?"selected":"" ?>>
                        Search by
                    </option>
                    <option value="email" <?= ($searchBy == "email")?"selected":"" ?>>
                        Email
                    </option>
                    <option value="color" <?= ($searchBy == "color")?"selected":"" ?>>
                        Color
                    </option>
                </select>
            </div>
            <div class="search">
                <input type="text" placeholder="Search..." name="search" value=<?= $search ?>>
                <button onclick="this.form.submit()" type="button">Search</button>
            </div>
        </form>
    </body>
</html>