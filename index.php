<?php

require "private/routes/Router.php";
require "private/request/Request.php";
require "private/controllers/PageController.php";
require "private/controllers/OrderController.php";

Router::load("private/routes/routes.php")->direct(Request::url(), Request::method());